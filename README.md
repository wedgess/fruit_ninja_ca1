# Game Summary
A fruit ninja game using Javascript - P5.js, MongoDB, NodeJS and Express. The game consists of three main screens. The login screen, game screen and high score screen. Each piece of fruit is worth one point, slicing a bomb deducts 10 points from the players score. Combos are achieved by slicing more than one fruit at the same time.

#### Login Screen
![Login Screen](https://i.imgur.com/5Cj46Oe.png)

#### Game Screen
![Game Screen](https://i.imgur.com/qmgh8BY.png)

#### High score Screen
![High score Screen](https://i.imgur.com/7uHJIjp.png)


# Prerequisite

--------------------------------------------------------------------------------

[MongoDB](https://docs.mongodb.com/manual/administration/install-community/) and [NodeJS](https://nodejs.org/en/download/package-manager/) are required for this project and must be **installed**. The following setup is for Linux.

Start mongo service:
```
sudo service mongod start
```

Enter mongoDB console:
```
mongo
```

Create fruit ninja database:
```
use fruitninja_scores
```

# Starting the game
-------------------------

MongoDB service must be running or started, the following command can be executed to check if mongo is running, if not the service will be started.

```
 ps -edaf | grep mongo | grep -v grep || sudo service mongod start
```

Now start the node server:
```
node server.js
```
