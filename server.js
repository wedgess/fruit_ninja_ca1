const express = require('express');
const MongoClient = require('mongodb').MongoClient;
const app = express();

// used for parsing body of requests
const bodyParser = require('body-parser');


// serve files from the public directory
app.use(express.static('public'));
app.use(bodyParser.urlencoded({
  extended: false
}));
app.use(bodyParser.json());

// connect to the db and start the express server
let db;

// ***Replace the URL below with the URL for your database***
const url = 'mongodb://localhost:27017/fruitninja_scores';

MongoClient.connect(url, (err, database) => {
  if (err) {
    return console.log(err);
  }
  db = database;
  // start the express web server listening on 8080
  app.listen(8080, () => {
    console.log('listening on 8080');
  });
});

// serve the login page
app.get('/', (req, res) => {
  res.sendFile(__dirname + '/index.html');
});

// serve the game page
app.get('/game', (req, res) => {
  console.log("Requested game")
  res.sendFile(__dirname + '/public/game.html');
});

// serve the score page
app.get('/scores', (req, res) => {
  console.log("Requested scores")
  res.sendFile(__dirname + '/public/scores.html');
});

app.post('/login', function(req, res) {
  let userName = req.body.name;

  // get user from db
  db.collection('fruitninja_scores').findOne({
    username: userName
  }, function(err, result) {
    if (err) return console.log(err);
    if (result) {
      console.log(`Logged in as: ${userName}`);
      res.redirect('/game');
    } else {
      // insert new user as user does not exist
      console.log("No user found, inserting user");
      // set initial score to 0
      db.collection('fruitninja_scores').insert({
        username: userName,
        score: 0
      }, (err, result) => {
        if (err) return console.log(err);
        console.log('user inserted/updated in db');
        res.redirect('/game');
      });
    }
  });
});


// add a document to the DB collection recording the high score
app.post('/fruitninja_scores', (req, res) => {
  //console.log(req);
  console.log("*** Request received: ", req.body);
  // update score by username
  db.collection('fruitninja_scores').update({
    username: req.body['username']
  }, req.body, {
    upsert: true
  }, (err, result) => {
    if (err) return console.log(err);
    console.log('high score updated in db');
    res.redirect('/');
  });
});

// get the high score data from the database
app.get('/fruitninja_scores/all', (req, res) => {
  // get username from query parameter
  db.collection('fruitninja_scores').find().sort({
    score: -1
  }).limit(3).toArray((err, result) => {
    if (err) return console.log(err);
    res.send(result);
  });
});

// get the high score data from the database
app.get('/fruitninja_scores', (req, res) => {
  // get username from query parameter
  userName = req.query.id;
  db.collection('fruitninja_scores').findOne({
    username: userName
  }, function(err, result) {
    if (err) return console.log(err);
    if (result) {
      console.log(result);
      res.send(result);
    }
  });
});
