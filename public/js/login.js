window.onload = function() {

  let usernameInput = document.getElementById('player__username');
  let submitBtn = document.getElementById('submit__login__btn');
  let errorText = document.getElementById('login__error__text');

  // on submit make sure username is not empty and not less than 3 characters long
  submitBtn.addEventListener('click', event => {
    let username = usernameInput.value;
    let error;

    if (!username) {
      error = "Username is empty";
    } else if (username.length < 3) {
      error = "Username too short";
    }

    // if error text is not empty set the error text, show it and prevent form submission
    if (error) {
      errorText.innerHTML = error;
      errorText.style.display = 'block';
      event.preventDefault();
    } else {
      // on login - store username in local storage so the username can be used to submit score in client.js
      localStorage.setItem("username", username);
    }
  });

  // listen to input on the username field so if error is displayed in clears in realtime
  usernameInput.addEventListener('input', event => {
    // if error text is displayed make sure both validation rules are met
    if (errorText.style.display === 'block' && usernameInput.value.length > 2) {
      errorText.style.display = 'none';
    }
  });

}
