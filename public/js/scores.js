window.onload = () => {
  let userHighscoreContainer = document.getElementById('users__highscore__container');
  let globalUserHighscoreContainer = document.getElementById('score__wrapper');
  let replayButton = document.getElementById('replay__btn');
  let userScore = document.getElementById('user__score');

  // set the users score from localstorage, if not available display "unknown"
  userScore.innerHTML = localStorage.getItem("score") || "unknown";

  // GET top high scores from database and display in a list
  fetch('/fruitninja_scores/all', {
      method: 'GET'
    })
    .then(response => {
      if (response.ok) return response.json();
      throw new Error('Request failed - getting high score');
    })
    .then(data => {
      console.log("Received high score data: ", data);
      data.forEach((user) => {
        globalUserHighscoreContainer.appendChild(new Player(user).getRow());
      });
    })
    .catch(error => {
      console.log("Error: ", error);
    });

  replayButton.addEventListener('click', event => {
    // redirect user to game page to replay game
    window.location.replace("http://localhost:8080/game");
  });
}
