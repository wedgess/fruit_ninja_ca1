console.log('Client-side code running');

// all fruits
const fruits = [];
let sword; // sword object
let score = 0; // total game score
let gameTime = 60; // game timer
let scoreMelonIcon; // game score icon (melon)
let startImageSixtySeconds;
let startImageGo;
let timeUpImage;
let gameBGImage;
let fireFruitSound;
let startGameSound;
let gameFont;
let startGameDelayTime = 4;
let startGameDelayCompleted = false;
let startGameDelayIntervalId;
let gameTimerIntervalId;
let highScore = 0; // the users highscore so we can see if they have beaten their current highscore
/*
 * Keep ref to sound globally accesible and not in fruit object so we can stop it if playing,
 * otherwise multiple sounds playing at once will crash the game.
 */
let fruitSplatterSound;
// get the username from local storage so we can submit the users score at game end
const userName = localStorage.getItem("username");


// callback for game timer
const startGameDelayTimer = () => {
  if (startGameDelayTime > 0) {
    startGameDelayTime--;
  } else {
    startGameDelayCompleted = true;
    gameTimerIntervalId = setInterval(gameTimer, 1000);
  }
}

// callback for game timer
const gameTimer = () => {
  if (gameTime > 0) gameTime--;
}

//////////////////// P5 functions ///////////////////////////
function preload() {
  soundFormats('mp3');
  fruitSplatterSound = loadSound('./assets/sound/splatter.mp3');
  fruitSplatterSound.setVolume(0.4);
  startGameSound = loadSound('./assets/sound/start.mp3');
  startGameSound.setVolume(0.6);
  fireFruitSound = loadSound('./assets/sound/throw.mp3');
  scoreMelonIcon = loadImage("./assets/images/score.png");
  gameBGImage = loadImage("./assets/images/background.jpg");
  startImageSixtySeconds = loadImage("./assets/images/60-seconds.png");
  startImageGo = loadImage("./assets/images/go.png");
  timeUpImage = loadImage("./assets/images/time-up.png");
  gameFont = loadFont("./assets/font/go3v2.otf");
  sword = new Sword();
  getHighScore();
}

// setup function
function setup() {
  createCanvas(900, 600);
  drawBG();
  // need to store the id so the interval can be cleared once start game images are shown
  startGameDelayIntervalId = setInterval(startGameDelayTimer, 1000);
}

// make draw function
function draw() {
  drawBG();
  if (startGameDelayCompleted) {
    if (startGameDelayIntervalId !== -1 && startGameDelayTime === 0) {
      clearInterval(startGameDelayIntervalId);
      startGameDelayIntervalId = -1;
    }
    if (gameTime === 0) {
      endGame();
    } else {
      addFruits();
      // check if any of the fruits have been sliced
      checkFruitSliced();
      sword.draw();
    }
    drawScore();
    drawGameTime();
    drawhighScore();
  } else {
    if (startGameDelayTime > 2) {
      if (!startGameSound.isPlaying()) {
        startGameSound.play();
      }
      image(startImageSixtySeconds, (width / 2) - (startImageSixtySeconds.width / 2), height / 2 - (startImageSixtySeconds.height / 2));
    } else {
      image(startImageGo, (width / 2) - (startImageGo.width / 2), height / 2 - (startImageGo.height / 2));
    }
  }
}

function mouseMoved() {
  // only track the mouse when over canvas
  if ((mouseX > 0 && mouseX < width) && (mouseY > 0 && mouseY < height)) {
    sword.move(mouseX, mouseY);
  } else {
    // if sword is not longer in canvas remove the points
    sword.clearPoints();
  }
}
///////////////////// Drawing score, time and game BG ////////////////////////
const drawBG = () => {
  image(gameBGImage, 0, 0, width, height);
}

const drawGameTime = () => {
  fill(gameTime <= 10 ? "#FF0000" : "#f5c82d");
  textFont(gameFont);
  textAlign(RIGHT);
  textSize(50);
  // position timer text on right hand side of game
  text(gameTime, width - 15, 45);
}

// draw score in top left corner
const drawScore = () => {
  image(scoreMelonIcon, 15, 15);
  textAlign(LEFT);
  fill("#f5c82d");
  textFont(gameFont);
  textSize(50);
  text(score, 55, 45);
}

// draw best score in top left corner below game score
const drawhighScore = () => {
  textAlign(LEFT);
  fill("#21d20f");
  textFont(gameFont);
  textSize(30);
  text(`Best: ${highScore}`, 10, 75);
}

function endGame() {
  noLoop(); // stop game loop, time up
  clearInterval(gameTimerIntervalId);
  image(timeUpImage, (width / 2) - (timeUpImage.width / 2), height / 2 - (timeUpImage.height / 2));
  localStorage.setItem("score", score);
  if (score > highScore) {
    submitHighScore(score);
  }
  setTimeout(() => {
    // redirect user to scores page
    window.location.replace("http://localhost:8080/scores");
  }, 2000);
}

// check collision of fruit and sword
function checkFruitSliced() {
  // draw splatters first so they are behind fruit items
  fruits.forEach(fruit => {
    if (fruit.isBomb) {
      // if bombs are hit they show -10 score image
      if (fruit.slice) {
        fruit.showHitScore();
      }
    } else {
      fruit.drawSplatter();
    }
  });
  /* Itterate backwards through array as array is being reindexed when
  doing a splice so an index will be skipped causing some fruit to
  skip frames */
  let i = fruits.length;
  while (i--) {
    let fruit = fruits[i];
    // fruit has gone off the screen, so remove from array
    if (!fruit.visible) {
      fruits.splice(i, 1);
    } else if (!fruit.sliced) {
      // if fruit is not sliced check fruit collision
      let points = sword.checkCollision(fruit);
      if (points > 0) {
        if (fruitSplatterSound.isPlaying()) {
          fruitSplatterSound.stop();
        }
        fruitSplatterSound.play();
      }
      // add score to of fruit to current game score
      score += points;
      // score can go below 0 if bombs are hit on low score
      if (score < 0) {
        score = 0;
      }
      // draw combo
      if (fruit.comboPosition) {
        fruit.drawCombo();
      }
      fruit.move().draw();
    }
  }
}

// Adding the generated fruit to current fruit array
function addFruits() {
  if (frameCount == 0 || frameCount % 70 === 0) {
    if (noise(frameCount) > 0.18) {
      // es6 add array to array
      fruits.push(...generateRandomFruit());
    }
  }
}

// Generating fruit to add and
function generateRandomFruit() {
  const randFruits = [];
  for (let i = 0; i < Math.round(random(2, 8)); i++) {
    // 0.86 is the probability of showing a bomb, add modulo to increase chance
    if (i % 2 == 0 && random() > 0.86) {
      randFruits.push(new Bomb());
    }

    let rand = random();
    if (rand <= 0.2) {
      randFruits.push(new Peach());
    } else if (rand <= 0.4) {
      randFruits.push(new Apple());
    } else if (rand <= 0.6) {
      randFruits.push(new Banana());
    } else if (rand <= 0.8) {
      randFruits.push(new Strawberry());
    } else if (rand <= 1) {
      randFruits.push(new Melon());
    }
  }
  // play sound when firing fruits
  if (fireFruitSound.isPlaying()) {
    fireFruitSound.stop();
  }
  fireFruitSound.play();
  return randFruits;
}

///////////////////////// Requests ////////////////////////////
const getHighScore = () => {
  fetch(`/fruitninja_scores?id=${userName}`, {
      method: 'GET'
    })
    .then(response => {
      if (response.ok) return response.json();
      throw new Error(`Request failed - caanot GET high score for ${userName}`);
    })
    .then(data => {
      console.log(`Received high score data: ${data}`);
      highScore = data['score'];
    })
    .catch(error => {
      console.log(`Error: ${error}`);
    });
}

const submitHighScore = score => {
  fetch('/fruitninja_scores', {
      method: 'POST',
      body: JSON.stringify({
        "username": userName,
        "score": score
      }),
      headers: new Headers({
        'Content-Type': 'application/json',
        Accept: 'application/json',
      })
    })
    .then(response => {
      if (response.ok) return;
      throw new Error('Request failed - cannot save score');
    })
    .catch(error => {
      console.log(`Error: ${error}`);
    });
}
