function Sword() {
  this.strokeColor = "#acd3db";
  this.width = 2;
  this.points = [];
  this.combo = 1;
  this.prevLastPoint; // we use this to check for combos
  this.noCheatPoint; // make sure user isn't sitting at single point
  setInterval(noCheatInterval.bind(this), 2000);
}

// prevent user from keeping sword in the same place for more than 2 seconds
const noCheatInterval = function() {
  if (this.points) {
    if (this.prevLastPoint === this.noCheatPoint) {
      this.clearPoints();
    } else {
      this.noCheatPoint = this.getLastPoint();
    }
  }
};

// draw all of the points in the array (called each frame so line stays displayed)
Sword.prototype.draw = function() {
  for (let index = 0; index < this.points.length; index++) {
    if (index > 1) {
      strokeWeight(10);
      stroke(this.strokeColor);
      line(this.points[index - 2].x, this.points[index - 2].y,
        this.points[index - 1].x, this.points[index - 1].y);
      // need to reset stroke so it is not applied to game text
      stroke(0);
      strokeWeight(1);
    }
  }
};

Sword.prototype.getLastPoint = function() {
  return this.points[this.points.length - 1];
};

// use simple box collision
Sword.prototype.checkCollision = function(fruit) {
  // if fruit is sliced or sword has no last point - ignore
  if (fruit.slice || !this.getLastPoint()) {
    return 0;
  }
  let point = this.getLastPoint();
  // check collision
  if ((point.x < (fruit.position.x + fruit.img.width) && point.x > fruit.position.x) &&
    (point.y < (fruit.position.y + fruit.img.height) && point.y > fruit.position.y)) {
    // if we chopped a fruit at same point as last one then we have a combo
    if (this.prevLastPoint && this.prevLastPoint.x == point.x &&
      this.prevLastPoint.y == point.y) {
      console.log("Combo");
      this.combo++;
    } else {
      // reset combo counter
      this.combo = 1;
    }
    this.prevLastPoint = point;
    fruit.setSliced(fruit.position.x, fruit.position.y);
    // if fruit are going up, make them fall when sliced
    if (fruit.vy < 0) {
      fruit.vy = fruit.vy * -1;
    }
    if (fruit.isBomb) {
      // save hit position of bomb to draw score
      fruit.hitPosition = createVector(fruit.position.x, fruit.position.y);
      return fruit.points;
    } else {
      // set combo image at position of combo
      if (this.combo > 1) {
        fruit.comboPosition = createVector(fruit.position.x, fruit.position.y);
      }
      // if there are combos multiply combo by 3 points otherwise 1 point
      return this.combo > 1 ? this.combo * 3 : fruit.points;
    }
  } else {
    return 0;
  }
};

Sword.prototype.move = function(x, y) {
  // keep at least 4 points to show sword trail/swipe
  if (this.points.length > 4) {
    this.points.splice(0, 1);
  }
  this.points.push(createVector(x, y));
};

// remove all points when sword goes outside of canvas
Sword.prototype.clearPoints = function() {
  this.points = [];
}
