function Fruit(imgSrc = "./assets/images/apple.png",
  splatter_array = ["./assets/images/splatter_apple1.png", "./assets/images/splatter_apple2.png"],
  split_array = ["./assets/images/apple-1.png", "./assets/images/apple-2.png"]) {
  Projectile.call(this, false, imgSrc, 1);
  this.splatterImg = loadImage(randomSplatterImg(splatter_array));
  this.splatPos;
  this.splatSize = random(0.5, 2.5);
  this.splitImgOne = loadImage(split_array[0]);
  this.splitImgTwo = loadImage(split_array[1]);
  // use closure
  this.splitPos = drawSplitImage(Math.round(random(30, 50)));
  this.comboImg = loadImage("./assets/images/combo.png");
  this.comboPosition;
}

// inherit properties from superclass Projectile
Fruit.prototype = Object.create(Projectile.prototype);

Fruit.prototype.drawCombo = function() {
  if (this.comboPosition) {
    image(this.comboImg, this.comboPosition.x, this.comboPosition.y);
  }
};

// override superclass function
Fruit.prototype.setSliced = function(x, y) {
  this.slice = true;
  this.splatPos = createVector(x, y);
  // change fruit image to one of the sliced fruit
  this.img = this.splitImgOne;
};

Fruit.prototype.drawSplatter = function() {
  if (this.slice) {
    tint(255, height / this.position.y * 80)
    image(this.splatterImg, this.splatPos.x, this.splatPos.y, this.splatterImg.width * this.splatSize, this.splatterImg.height * this.splatSize)
    noTint();
    // draw the second split image as the fruit has been sliced
    image(this.splitImgTwo, this.splitPos(this.position.x), this.position.y)
  }
};

// pick a random splatter image
const randomSplatterImg = splatter_array => splatter_array[Math.floor(random(0, 2))];
// closure for drawing the split images x position
const drawSplitImage = offset => x => x - offset;
