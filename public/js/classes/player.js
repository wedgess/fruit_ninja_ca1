// template for each player row  -- name and score
const viewTemplate = '<div class="player__score__row">\
                        <div class="player__name">{{name}}</div>\
                        <div class="player__score">{{score}}</div>\
                        </div>\
                    </div>';

function Player(player) {
  this.username = player.username;
  this.score = player.score;
}

Player.prototype.getRow = function() {
  // use template element
  var playerListItem = document.createElement('template');
  // replace name placeholder inside the template with this players name
  var playerListTemplate = viewTemplate.replace(/{{name}}/g, `${this.username}:  `).replace(/{{score}}/g, this.score);

  // set the innerHTML on the player list
  playerListItem.innerHTML = playerListTemplate;

  // return the firstChild of the template (the div)
  return playerListItem.content.firstChild;
}
