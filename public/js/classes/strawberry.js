function Strawberry() {
  Fruit.call(this, "./assets/images/strawberry.png",
  ["./assets/images/splatter_strawberry1.png", "./assets/images/splatter_strawberry2.png"],
  ["./assets/images/strawberry-1.png", "./assets/images/strawberry-2.png"]);
}

// inherit properties from superclass Fruit
Strawberry.prototype = Object.create(Fruit.prototype);
