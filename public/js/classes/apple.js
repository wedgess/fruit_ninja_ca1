function Apple() {
  Fruit.call(this, "./assets/images/apple.png",
  ["./assets/images/splatter_apple1.png", "./assets/images/splatter_apple2.png"],
["./assets/images/apple-1.png", "./assets/images/apple-2.png"]);
}

// inherit properties from superclass Fruit
Apple.prototype = Object.create(Fruit.prototype);
