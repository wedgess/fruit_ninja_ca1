function Peach() {
  // call fruit constructor
  Fruit.call(this, "./assets/images/peach.png",
  ["./assets/images/splatter_peach1.png", "./assets/images/splatter_peach2.png"],
["./assets/images/peach-1.png", "./assets/images/peach-2.png"]);
}

// inherit properties from superclass Fruit
Peach.prototype = Object.create(Fruit.prototype);
