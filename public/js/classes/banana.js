function Banana() {
  Fruit.call(this, "./assets/images/banana.png",
  ["./assets/images/splatter_banana1.png", "./assets/images/splatter_banana2.png"],
["./assets/images/banana-1.png", "./assets/images/banana-2.png"]);
}

// inherit properties from superclass Fruit
Banana.prototype = Object.create(Fruit.prototype);
