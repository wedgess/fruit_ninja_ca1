function Projectile(isBomb = false, imgSrc = "./assets/images/apple.png", points = 1) {
  this.img = loadImage(imgSrc);
  this.position = createVector(
    random(this.img.width, height - this.img.height),
    height + this.img.height
  );
  this.isBomb = isBomb;
  this.slice = false;
  this.visible = true;
  this.points = points; // game points
  // make velocity always point towards center
  this.vx = this.position.x > width / 2 ? random(-6, -0.5) : random(0.5, 6);
  // random max height
  this.vy = random(-12, -17);
}

Projectile.prototype.draw = function() {
  image(this.img, this.position.x, this.position.y)
};

// moves the projectile
Projectile.prototype.move = function() {
  this.vx *= 0.99; // air resistance
  this.vy += 0.25; // gravity
  this.position.x += this.vx;
  this.position.y += this.vy;
  this.visible = this.position.y < height + this.img.height;
  return this;
};
