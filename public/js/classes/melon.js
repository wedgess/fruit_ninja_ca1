function Melon() {
  Fruit.call(this, "./assets/images/melon.png",
  ["./assets/images/splatter_melon1.png", "./assets/images/splatter_melon2.png"],
["./assets/images/melon-1.png", "./assets/images/melon-2.png"]);
}

// inherit properties from superclass Fruit
Melon.prototype = Object.create(Fruit.prototype);
