function Bomb() {
  // bomb uses bomb image and when sliced is -10 points
  Projectile.call(this, true, "./assets/images/bomb.png", -10);
  this.splatter = loadSound('./assets/sound/bomb.mp3');
  this.splatter.setVolume(0.8);
  this.showScore = loadImage("./assets/images/bomb-hit.png");
  this.hitPosition;
}

// inherit properties from superclass Fruit
Bomb.prototype = Object.create(Projectile.prototype);

// override superclass function
Bomb.prototype.setSliced = function(x, y) {
  this.slice = true;
  this.splatter.play();
};

Bomb.prototype.showHitScore = function() {
  image(this.showScore, this.hitPosition.x, this.hitPosition.y);
}
